#!/usr/bin/env bash

# This file is referenced from renovate-common.json, in the
# postUpgradeTasks.commands, and is intended to be called within
# the renovate container image, following an upgrade.
# Because postUpgradeTasks can't be merged together, we run all
# common upgrade tasks from a single script.
#
# The script takes a two arguments, `{{{depName}}}` and `{{{packageFile}}`
# which are resolvedby renovate:
# https://docs.renovatebot.com/templates/
#
# ASDF_DIR should be configured, but will default to /asdf

set -euo pipefail
IFS=$'\n\t'

dep_name=$1
package_file=$2

main() {
  echo "post-renovate-upgrade.sh updating repository..."

  if [[ -x ./scripts/update-asdf-version-variables.sh ]]; then
    echo "Running ./scripts/update-asdf-version-variables.sh..."

    ./scripts/update-asdf-version-variables.sh
  fi

  ASDF_DIR=${ASDF_DIR:-/asdf} # ASDF location in container is /asdf
  # shellcheck source=/dev/null
  source "${ASDF_DIR}/asdf.sh"

  # When shfmt is updated, rerun the command to apply any new rules
  if { [[ $dep_name == 'mvdan/sh' ]] || [[ $dep_name == 'shfmt' ]]; } && [[ $package_file == '.tool-versions' ]]; then
    echo "Reapplying shfmt formatting for updated version..."
    asdf plugin add shfmt || true
    asdf install shfmt
    (shfmt --find . |
      grep -vE "${COMMON_TASK_VALIDATIONS_EXCLUDES_REGEXP:-__ignored__}" |
      xargs shfmt -w -l -s -i 2 -d) || true # Don't treat a failed exit code as a script failure
  fi

  if [[ $dep_name == "golangci/golangci-lint" ]]; then
    echo "Reapplying golangci/golangci-lint with --fix to fix any new issues..."
    asdf plugin add golangci-lint || true
    asdf install golangci-lint
    golangci-lint run --fix || true # Don't treat a failed exit code as a script failure
  fi
}

main
