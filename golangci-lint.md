
# [`golangci-lint`](./golangci-lint.yml)

Runs [golangci-lint](https://github.com/golangci/golangci-lint) on the project.

* Stages: `validate`

```yaml
stages:
  - validate

include:
  # Runs golangci-lint on the project.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/golangci-lint.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.1.1  # renovate:managed
    file: 'golangci-lint.yml'
```
