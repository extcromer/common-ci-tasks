# Environment Setup

This page describes the common process for setting up the common local development and workstation setup used across many Infrastructure projects.

## Setup

### Step 1: Install 1password-cli v2

In order to protect sensitive values, many GitLab projects rely on 1password for storing secrets. This may be either directly, or through internal tooling such as [`pmv`](https://gitlab.com/gitlab-com/gl-infra/pmv).

Install 1password CLI using the instructions at: <https://developer.1password.com/docs/cli/get-started#install>. Unfortunately version management or package management tools cannot be used due to the security requirements of the 1password client.

For macos, you can use:

```shell
brew install --cask 1password/tap/1password-cli
```

Once 1password-cli is installed you can verify the installation using:

```shell
op --version
```

**Note: v2 of the 1password-cli is required.**

### Step 2: Setup Development Libraries

The Infrastructure group uses some Python-based tools, including the [`gcloud` CLI](https://cloud.google.com/sdk/gcloud) and the [AWS CLI](https://aws.amazon.com/cli/), [`pre-commit`](https://pre-commit.com/) and [Ansible](https://www.ansible.com/).

We've found that in some cases, Python will silently drop required modules (eg `sqlite3`) when the library dependencies of those modules are not installed on your system. This can lead to systems failing in unusual ways at runtime.

It is recommended that you follow the instructions to install the suggested build environment for Python, here: <https://github.com/pyenv/pyenv/wiki#suggested-build-environment>.

Linux users can follow the instructions for their distro on that site. For Macos users, the following set of commands should ensure the prerequisites.

```shell
# Ensure that xcode build tools are installed
xcode-select --install
# Ensure required libraries for python
brew install openssl readline sqlite3 xz zlib
```

### Step 2.5: Apple Silicon users only: install Rosetta

> To determine if you are using an Apple with Silicon or an Intel-based processor, select the **Apple** menu icon in the top-left of the desktop and click **About This Mac**. On Mac computers with Apple silicon, About This Mac shows an item labeled **Chip**, followed by the name of the chip (ex. **Chip** Apple M1 Pro). On Mac computers with an Intel processor, About This Mac shows an item labeled **Processor**, followed by the name of an Intel processor.

If you are running Apple Silicon it's highly recommended that you install Rosetta. Many downstream tools still generate `amd64` binaries only, and even if the tools support `arm64`, `asdf` plugins also need to support it, which they may not yet.

**Note**: You will likely hit many problems unless Rosetta is installed.

Check if Rosetta is installed by checking if it is running with the following one-liner:

```shell
pgrep -q oahd && echo "Rosetta is present on the system and is running." || echo "Rosetta is not running."
```

If it is not running, it is likely that it is not installed, so continue with the installation steps:

```shell
softwareupdate --install-rosetta --agree-to-license
```

### Step 3.1: Uninstall `asdf` before installing `rtx`

The Infrastructure department is currently migrating from `asdf` to `rtx`. See [gitlab-com/runbooks#134](https://gitlab.com/gitlab-com/runbooks/-/issues/134) for details of the migration. Many projects now support both `rtx` and `asdf`, but if you're experiencing errors running `scripts/install-asdf-plugins.sh` on a project, please leave a comment on [gitlab-com/runbooks#134](https://gitlab.com/gitlab-com/runbooks/-/issues/134).

**It is not recommended that you run `asdf` alongside `rtx`.**

Please ensure that you uninstall `asdf` before installing `rtx`.

To uninstall `asdf`, full instructions are available at <https://asdf-vm.com/manage/core.html#uninstall>.

For MacOS users who installed `asdf` with Homebrew, remove any includes of `asdf` from your Shell initialization, then use Homebrew to remove `asdf`:

```console
$ # find asdf references in your shell rc files
$ grep -Er 'asdf.(ba)?sh' ~/.bash_profile ~/.bashrc ~/.zshrc ~/.oh-my-zsh/custom/*
$ # edit the files to delete any references
$ vi ...
$ # now, uninstall asdf
$ brew uninstall asdf --force
$ # open a new terminal before continuing...
```

In addition to removing references to loading asdf in your rc files, you may need to remove compile time environment variables such as LDFLAGS, RUBY_CONFIGURE_OPTS, CPPFLAGS, and PKG_CONFIG_PATH.

### Step 3.2: Install `rtx`

[`rtx`](https://github.com/jdx/rtx) is a polyglot runtime manager. It is compatible with [`asdf`](https://asdf-vm.com/) and relies on the `asdf` plugin ecosystem, but has some advantages over `asdf` in that it generally requires fewer shims and is faster.


If you're running on MacOS, the recommended approach is to use Homebrew:

```shell
brew install rtx
```

Linux users should follow the instructions for their package manager in [the rtx documentation](https://github.com/jdx/rtx#apt).

#### Step 3.3: Hook `rtx` into your shell

Once you've installed `rtx`, add the following line to your shell. Remember to restart your terminal for the change to take affect.

```shell
# bash
echo 'eval "$('$(which rtx)' activate bash)"' >> ~/.bashrc
# zsh
echo 'eval "$('$(which rtx)' activate zsh)"' >> ~/.zshrc
# fish
echo 'rtx activate fish | source' >> ~/.config/fish/config.fish
```

Did you remember to restart your terminal? Good.

### Step 2: Install development dependencies

Install all the plugins by running:

```shell
./scripts/install-asdf-plugins.sh
```

This will install required `asdf`/`rtx` plugins, and install the correct versions of the development tools.

Note that after pulling changes to the repository, you may sometimes need to re-run `./scripts/install-asdf-plugins.sh` to update your locally installed plugins and tool-versions.

## Updating Tool Versions

We use CI checks to ensure that tool versions used in GitLab-CI and on developer instances don't go out of sync.

### Keeping Versions in Sync between GitLab-CI and `.tool-versions`.

The `.tool-versions` file is the SSOT for tool versions used in this repository.
To keep `.tool-versions` in sync with `.gitlab-ci.yml`, there is a helper script,
`./scripts/update-asdf-version-variables.sh`.

#### Process for updating a Tool Version

```shell
./scripts/update-asdf-version-variables.sh
```

1. Update the version in `.tool-versions`
1. Run `rtx install` to install latest version
1. Run `./scripts/update-asdf-version-variables.sh` to update a refresh of the `.gitlab-ci-asdf-versions.yml` file
1. Commit the changes
1. A CI job (see [`asdf-tool-versions.md`](../asdf-tool-versions.md)) will validate the changes.

# Diagnosing `rtx` setup issues

## `asdf`/`rtx` plugins don't return versions as expected

To avoid installing dependencies, `asdf`/`rtx` plugins often rely on basic Unix text processing utilities like `grep`, `sed` and `awk` to parse JSON. Many rely on the fact that responses from the GitHub API are pretty-printed JSON, not minimised (or machine parsable) JSON. However, the GitHub API will only pretty-print JSON when it detects the User-Agent request header as being `curl`. For other user agents, the response will be minimised for efficiency.

Ensure that you haven't overridden your `curl` user-agent on your workstation.

Check your `.curlrc` for the `user-agent` setting. Additionally, running `curl https://api.github.com/orgs/gitlabhq` should return pretty-printed JSON. If the response contains minimised JSON, many `asdf`/`rtx` plugins may not work as expected.

## `asdf`/`rtx` plugins require other `asdf`/`rtx` plugins

If you find that a plugin is failing to install, it sometimes helps to setup a global default. Care has been taken to avoid this situation, but if you're stuck, give it a try:

```shell
# If a plugin is complaining that it cannot compile because golang hasn't been configured...
# set the global version of golang the same as the current version
rtx global golang $(rtx current golang)
rtx install
```

If this happens, please open an issue in the appropriate tracker, so that a better long-term solution can be applied.
