# [`renovate-bot`](./renovate-bot.yml)

Runs [`renovatebot`](https://docs.renovatebot.com/) against the project to automatically upgrade dependencies.

1. Ensure that a `validate` and `renovate_bot` stages exists in your `.gitlab-ci.yml` configuration
1. Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api` and `write_repository` scope and make it available in your CI environment via the `RENOVATE_GITLAB_TOKEN` environment variable through the CI/CD Variables settings. Make sure the variable is **Protected** and **Masked**.
1. Lookup the GitHub Personal Access token from [1Password Production Vault](https://gitlab.1password.com/vaults/7xbs54owvjux3cypztlhyetej4/allitems/53z2zuf7urh7hoy3nqeqsei27e) and save it into the CI environment variable `RENOVATE_GITHUB_TOKEN`. Make sure the variable is **Protected** and **Masked**.
1. Note that you can use Group Access Tokens and Group CI/CD variables, instead of Project-level ones should you choose.
1. Create a CI Pipeline Schedule called `Renovatebot` with a daily schedule, eg `0 1 * * *`. Ensure that the CI Pipeline schedule includes a variable, `RENOVATE_SCHEDULED` with a value of `1`.
1. Note that Renovate Bot will only run on `gitlab.com`. For projects that are mirrored to other GitLab instances, the task will not run.
1. Create a `renovate.json` file in the root of the project. See below for an example configuration:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": ["gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common"]
}
```

```yaml
stages:
  - validate
  - renovate_bot

include:
  # Upgrades dependencies on a schedule
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.1.1  # renovate:managed
    file: renovate-bot.yml
```

## Annotating `.tool-versions` and `.gitlab-ci-other-versions.yml` to support Renovate

The preset [`gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common`](./renovate-common.json) includes configuration to upgrade ASDF `.tool-versions` and other version changes in `.gitlab-ci-other-versions.yml`. For `.tool-versions`, its expected that renovate will support this natively in future, but for now, this workaround should suffice.

In order to assist Renovate in understanding what each dependency in `.tool-versions` is, we need to add a comment, like this:

```shell
golang 1.17.6 # datasource=github-releases depName=golang/go
goreleaser 1.7.0 # datasource=github-releases depName=goreleaser/goreleaser
golangci-lint 1.45.1 # datasource=github-releases depName=golangci/golangci-lint
shellcheck 0.8.0 # datasource=github-releases depName=koalaman/shellcheck
shfmt 3.4.3 # datasource=github-releases depName=mvdan/sh
1password-cli 1.12.4
```

Similarly, for `.gitlab-ci-other-versions.yml`,

```yaml
variables:
  CONTAINER_IMAGE_VERSION_PREFIXED: "v1.13.1" # datasource=gitlab-releases depName=gitlab-com/container-image
```

Renovate supports multiple datasources: <https://docs.renovatebot.com/modules/datasource/>, giving flexibility in how new versions are looked up.

### Using `registryUrl` for Helm Chart Renovate Annotations

When annotating for [`helm`](https://docs.renovatebot.com/modules/datasource/#helm-datasource) dependency updates, the special `registryUrl=https://chart/url` syntax can be added to the annotation to allow to configure the appropriate Helm repository to query.

For example, the following annotation can be used for the `kube-prometheus-stack` Helm chart.

```yaml
# datasource=helm depName=kube-prometheus-stack registryUrl=https://prometheus-community.github.io/helm-charts
```

More details can be found in <https://github.com/renovatebot/renovate/issues/6130>.

### Example Annotations

For convenience, here are some common annotations that can be cut-and-pasted into your projects:

| **Tool** | **Annotation** |
|----------| ----------|
| `awscli` | `# datasource=github-tags depName=aws/aws-cli` |
| `checkov` | `# datasource=github-releases depName=bridgecrewio/checkov` |
| `direnv` | `# datasource=github-releases depName=direnv/direnv` |
| `gcloud` | `# datasource=github-tags depName=GoogleCloudPlatform/cloud-sdk-docker` |
| `go-jsonnet` | `# datasource=github-releases depName=google/go-jsonnet` |
| `golang` | `# datasource=golang-version depName=golang/go` |
| `golangci-lint` | `# datasource=github-releases depName=golangci/golangci-lint` |
| `goreleaser` | `# datasource=github-releases depName=goreleaser/goreleaser` |
| `helm` | `# datasource=github-releases depName=helm/helm` |
| `jb` | `# datasource=github-releases depName=jsonnet-bundler/jsonnet-bundler` |
| `jq` | `# datasource=github-releases depName=stedolan/jq` |
| `jsonnet-tool` | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/jsonnet-tool` |
| `kube-prometheus-stack` | `# datasource=helm depName=kube-prometheus-stack registryUrl=https://prometheus-community.github.io/helm-charts` |
| `kubectl` | `# datasource=github-releases depName=kubernetes/kubernetes` |
| `nodejs` | `# datasource=node depName=nodejs/node` |
| `pmv` | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/pmv` |
| `pre-commit` | `# datasource=github-releases depName=pre-commit/pre-commit` |
| `promtool` | `# datasource=github-releases depName=prometheus/prometheus` |
| `python` | `# datasource=github-tags depName=python/cpython` |
| `ruby` | `# datasource=ruby-version depName=ruby/ruby` |
| `shellcheck` | `# datasource=github-releases depName=koalaman/shellcheck` |
| `shfmt` | `# datasource=github-releases depName=mvdan/sh` |
| `terra-transformer` | `# datasource=gitlab-releases depName=gitlab-com/gl-infra/terra-transformer` |
| `terraform` | `# datasource=github-releases depName=hashicorp/terraform` |
| `tflint` | `# datasource=github-releases depName=terraform-linters/tflint` |
| `thanos` | `# datasource=github-releases depName=thanos-io/thanos` |
| `yq` | `# datasource=github-releases depName=mikefarah/yq` |

## Forcing renovate to run off-schedule

To force renovate to create MRs immediately, run a pipeline with `RENOVATE_IMMEDIATE=1` set. This will override the default Renovate schedule.

## Automerge (Experimental)

In an effort to reduce toil, a [Renovate Automerge configuration](https://docs.renovatebot.com/key-concepts/automerge/) is being experimented with. To opt-in, add `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-automerge"` to the `extends` clause of your `renovate.json`.

This will automatically configure development-time dependency to be auto-merged if the pipeline succeeds.

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common",
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-automerge"
  ]
}
```

## Standard Version Constraints

By default, Renovate will attempt to update all versions to the latest. However, there may be external constraints on versions. For example, the version of Kubernetes dependencies should not exceed the version of Kubernetes running in production by more than one minor version.

To DRY-up the control of these version constraints, two presets are available:

1. **For GitLab.com version constraints**: `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-versions-gitlab-com"`.
1. **For GitLab Dedicated version constraints**: `"gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-versions-dedicated"`.

Please ensure that these dependencies are kept in sync with infrastructure by updating the dependencies when upgrades to infrastructure is carried out.

## Terraform Module Management

It's considered a security best practice to pin Terraform modules to a specific version to ensure that any changes to the module are reviewed and approved before being applied to production. Additionally, as tags are considered mutable in Git, it's recommended to leverage the digest of a particular version as opposed to the tag. Using the digest will help prevent [supply chain attacks](https://medium.com/boostsecurity/erosion-of-trust-unmasking-supply-chain-vulnerabilities-in-the-terraform-registry-2af48a7eb2) or any unintentional changes by the module author.

To allow Renovate to manage updating the terraform modules pinned to digest, simply annotate the `file.tf` with the following:

```hcl
module "project_services" {
  #renovate: version=v14.2.0
  source = "git::https://github.com/terraform-google-modules/terraform-google-project-factory.git//modules/project_services?ref=84ade5e6a32241128ac39a66dd5bf6184eb84043"
}
```

By default this Renovate config leverages the `github-tags` datasource to look for updates but if your module is hosted elsewhere or requires a different datasource, simply update the annotation like so:

```hcl
module "project_services" {
  #renovate: version=v14.2.0 datasource=gitlab-tags
  source = "git::https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules.git//terraform-modules/gcp-oidc?ref=2cb4111e026b3a18ff1271a4665b3015a278ea22"
}
```
**Note**: The `regexManager` used to support this is a temporary workaround until Renovate supports this natively. See [this issue](https://github.com/renovatebot/renovate/issues/23248) for more details and up-to-date status.
